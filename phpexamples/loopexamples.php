<?php

//for
// for($i=1; $i<=10; $i++)
// {
//     echo "<br />$i";

// }

// for($i=1; $i<=10; $i++):
//     echo "<br />$i";
// endfor;

//while
$start=5;
// while($start<=10){
//     echo "<br />value of start: $start";
//     $start++;
// }

// while($start<11):
// echo "<br />value of start: $start";
// $start++;
// endwhile;

//do..while
// do{
//     echo "<br />value of start: $start";
//     $start--;
// }while($start>1);

for($b=4; $b<=8; $b++):
    echo "<br /><input type='text' placeholder='Question No $b' /> <br />";
endfor;


?>