<?php

$menu=[
    'Home'=>'index.php',
    'Student Register'=>'stdregister.php',
    'bill'=>'bill.php',
    'receipt'=>'receipt.php',
    'New User'=>'newuser.php',
    'Login'=>'login.php',
];

?>


<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Timaade</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        
          
<?php  foreach ($menu as $key => $value):?>
<li class="nav-item">
    <a class="nav-link active" aria-current="page" href="<?= $value ?>"><?= $key ?></a>
</li>
<?php endforeach  ?>
      
      </ul>
    </div>
  </div>
</nav>