<?php
require_once('includes/header.php');
require_once('connection.php');

$showerror=false;
$errormsg="";

if($_SERVER["REQUEST_METHOD"]=="POST" && isset($_POST["submit"])){

    //read data
    $name = $_POST["txtname"];
    $mobile = $_POST["txtmobile"];
    $mother = $_POST["txtmother"];
    $class = $_POST["txtclass"];

    $sql ="INSERT INTO STUDENTS (NAME, MOBILE, MOTHER, CLASS) VALUES('$name','$mobile','$mother','$class');";
    if($conn->query($sql)==false){
        $showerror=true;
        $errormsg="Error Occured: ".$conn->error;
    }
}


$q="SELECT * FROM STUDENTS ORDER BY ID DESC";
$result = $conn->query($q);


$counter=1;


?>

<div class="row col-12">
    <h2>Register Student</h2>
</div>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
<div class="row">
    <div class="col-lg-4">
        <input type="text" class="form-control my-3"  placeholder="Enter Fullname" name="txtname">
        <input type="text" class="form-control my-3" placeholder="Enter Mobile" name="txtmobile">
        <input type="text" class="form-control my-3" placeholder="Enter Mother Name" name="txtmother">
        <input type="text" class="form-control my-3" placeholder="Enter Class" name="txtclass">
        <input type="submit" class="btn btn-primary" value="Submit Data" name="submit" >
        <br />
        <?php if($showerror):?>
            <div class="alert alert-danger my-3" role="alert">
                <?= $errormsg; ?>
            </div>
        <?php endif;?>
        


    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-7">
        <?php if($result->num_rows>0):?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th>Mother</th>
                        <th>Class</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php while($row=$result->fetch_assoc()):?>
                        <tr>
                            <td><?= $counter?></td>
                            <td><?= $row["name"]?></td>
                            <td><?= $row["mobile"]?></td>
                            <td><?= $row["mother"]?></td>
                            <td><?= $row["class"]?></td>
                            <td>
                                <a class="btn btn-success" href="stdedit.php?id=<?= $row["id"]?>">Edit</a>
                                <a class="btn btn-danger" href="stddelete.php?id=<?= $row["id"]?>">Delete</a>
                            </td>
                        </tr>

                        <?php $counter++; ?>
                    <?php endwhile;?>
                    
                </tbody>
            </table>

        <?php else:?>
            <h3>No Data Found !</h3>
        <?php endif;?>
    </div>
</div>
    
    
</form>



<?php
require_once('includes/footer.php');
?>