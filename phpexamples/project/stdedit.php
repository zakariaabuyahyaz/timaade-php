<?php
require_once('includes/header.php');
require_once('connection.php');

$showerror=false;
$errormsg="";

if($_SERVER["REQUEST_METHOD"]=="POST" && isset($_POST["submit"])){

    //read data
    $id = $_POST["stdid"];
    $name = $_POST["txtname"];
    $mobile = $_POST["txtmobile"];
    $mother = $_POST["txtmother"];
    $class = $_POST["txtclass"];

    $sql ="UPDATE STUDENTS SET NAME='$name', MOBILE='$mobile', MOTHER='$mother', CLASS='$class' WHERE ID=$id; ";
    if($conn->query($sql)==false){
        $showerror=true;
        $errormsg="Error Occured: ".$conn->error;
    }else{
        header("Location:stdregister.php");
    }
}

if(isset($_GET["id"]))
{
    $id = $_GET["id"];
    $q="SELECT * FROM STUDENTS WHERE ID=$id";
    $result = $conn->query($q);
    $singlestudent = $result->fetch_assoc();

}


?>

<div class="row col-12 my-3">
    <h2>Edit Student</h2>
</div>

<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
<div class="row">
    <div class="col-lg-4">
        <input type="hidden" name="stdid"  value="<?= $singlestudent["id"] ?>">
        <input type="text" value="<?= $singlestudent["name"] ?>" class="form-control my-3"  placeholder="Enter Fullname" name="txtname">
        <input type="text"  value="<?= $singlestudent["mobile"] ?>" class="form-control my-3" placeholder="Enter Mobile" name="txtmobile">
        <input type="text"  value="<?= $singlestudent["mother"] ?>" class="form-control my-3" placeholder="Enter Mother Name" name="txtmother">
        <input type="text"  value="<?= $singlestudent["class"] ?>" class="form-control my-3" placeholder="Enter Class" name="txtclass">
        <input type="submit" class="btn btn-primary" value="Edit" name="submit" >
        <br />
        <?php if($showerror):?>
            <div class="alert alert-danger my-3" role="alert">
                <?= $errormsg; ?>
            </div>
        <?php endif;?>
        


    </div>
    <div class="col-lg-1"></div>
    <div class="col-lg-7">
       
    </div>
</div>
    
    
</form>



<?php
require_once('includes/footer.php');
?>