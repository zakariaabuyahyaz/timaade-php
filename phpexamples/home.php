<?php
session_start();
if(!isset($_SESSION["username"])){
    header("Location:login.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<?php if(isset($_SESSION["username"])) :?>
<div style="background-color:pink; font-size:20px; ">
<h3>New Remittence</h3>
<hr />
<h4>Welcome Mr/Mrs: <?php echo $_SESSION["username"] ?>  <a href="logout.php">Logout</a></h4>
<form action="">
    <table>
        <tr>
            <td width="25%"><label for="">Customer Name:</label></td>
            <td width="75%"><input type="text" name="txtcustomer"/></td>
        </tr>
        <tr>
            <td width="25%"><label for="">Customer Amount:</label></td>
            <td width="75%"><input type="text" name="txtamount"/></td>
        </tr>
        <tr>
            <td width="25%"></td>
            <td width="75%"><input type="submit" value="Send Money" /></td>
        </tr>
</table>

</form>
</div>
<?php endif; ?>
    
</body>
</html>