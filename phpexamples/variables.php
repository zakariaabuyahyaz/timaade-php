<?php

// create variable
$age = 30;   //implicit
$price = 125.6;

// var_dump($price);  // see details of variable
// echo $price;   //send the value to output

// echo "<br />";
$username = "Alijama";
// var_dump($username);

$is_logged = false;
// var_dump($is_logged);

////  conditions
// if (3 > 2) {
//     echo "3 is greater";
// } else {
//     echo "2 is greater";
// }

$marks = "C";
// if ($marks == "A") {
//     echo "Marks is A grade";
// } elseif ($marks == "B") {
//     echo "Marks is B grade";
// } else {
//     echo "Other Marks";
// }

// switch ($marks) {
//     case "A":
//         echo "A Grade";
//         break;
//     case "B":
//         echo "B";
//         break;
//     default:
//         echo "Other Marks";
//         break;

// }

$msg = (10 > 5) ? "10 is greater" : "5 is greater";  //ternary operator
// echo $msg;

// $logged_user = "Ahmed";
// $uname = $logged_user ?? "Guest";
// echo "Welcome, $uname!";

// $product_price = 23.6;
// $x = $product_price ?? 0;
// echo "This product price is: $x";

// $netprice = "52.62598520";
// $nprice = (float) $netprice;
// $nprice = floatval($nprice);
// var_dump($nprice);

// $str_price = 20.6;
// $l = strval($str_price);  //(string)
// var_dump($l);

// $is_logged = null;    // empty string, 0, null
// $il = (bool) $is_logged;
// var_dump($il);

$students = array("ahmed", "ali", "jama");
$oa = (object) $students;
var_dump($oa);



?>